Projet: API TypeScript avec Node.js
===================================

Ce projet présente une simple API réalisée avec TypeScript et Node.js. Elle retourne les headers de la requête au format JSON lors d'une requête HTTP GET sur l'endpoint `/ping`.

Caractéristiques :
------------------

*   Utilise le système de typage de TypeScript pour réduire les erreurs.
*   Compatible avec l'écosystème JavaScript, mais avec une syntaxe améliorée.
*   Contrôle la cohérence des types pendant le développement et lors du build.
*   Peut être utilisé à la fois sur le frontend et le backend.
*   Le JavaScript transpilé est compatible avec des navigateurs anciens.

Initialisation du projet :
--------------------------

Suivez ces étapes pour initialiser le projet :

bash

```bash
cd my_project
npm init -y
npm install typescript --save-dev
npm install @types/node --save-dev
npx tsc --init --rootDir src --outDir build --esModuleInterop --resolveJsonModule --lib es6 --module commonjs --allowJs true --noImplicitAny true
mkdir src
echo "console.log('Hello world')" > src/index.ts
npx tsc
node build/index.js
```

Configuration :
---------------

L'API écoute sur un port défini par la variable d'environnement `PING_LISTEN_PORT`. Si cette variable n'est pas définie, le port par défaut est `8080`.

Utilisation :
-------------

Après avoir démarré le serveur, faites une requête GET sur `/ping` pour obtenir les headers de la requête au format JSON. Toute autre requête renverra une réponse vide avec le code `404`.

Conseils :
----------

*   Pas besoin de gérer la mémoire ou l'appartenance des variables avec TypeScript. Utilisez le Guide Officiel TypeScript pour vous aider.
*   Essayez de minimiser les dépendances externes pour réduire les vulnérabilités potentielles.

Liens utiles :
--------------

*   [Guide Officiel TypeScript](https://www.typescriptlang.org/fr/docs/handbook/typescript-from-scratch.html)
*   [Setup node TypeScript project](https://www.digitalocean.com/community/tutorials/setting-up-a-node-project-with-typescript)
*   [Node starter project](https://khalilstemmler.com/blogs/typescript/node-starter-project/)
*   [Create a TypeScript, NodeJS, and Express web server example](https://www.linode.com/docs/guides/using-nodejs-typescript-and-express-to-build-a-web-server/)

Dépendances :
-------------

Une dépendance est un code ou une solution externe essentielle au projet. Dans ce projet, nous utilisons le travail de la communauté à travers `typescript` et `@types/node`. Le code externe est accessible depuis des dépôts comme npm. Notez que l'ajout de dépendances externes ajoute un maillon supplémentaire dans la chaîne d'approvisionnement de l'application.

---

Vous pouvez maintenant ajouter ou ajuster les sections en fonction des besoins spécifiques de votre projet ou des préférences de votre équipe.

> refait en explicant l'api comment la start et tout

Bien sûr, je vais refaire le README en me concentrant sur la description, l'utilisation, et la mise en route de l'API.

---

API TypeScript pour Headers avec Node.js
========================================

Ce projet est une API simple conçue avec TypeScript et Node.js. Lorsqu'une requête HTTP GET est envoyée à l'endpoint `/ping`, l'API renvoie les headers de la requête au format JSON.

Caractéristiques :
------------------

*   **Endpoint `/ping`** : Lors d'une requête GET, renvoie les headers de la requête.
*   **Port Configurable** : Utilise la variable d'environnement `PING_LISTEN_PORT` ou, par défaut, le port `8080`.

Prérequis :
-----------

*   Node.js installé sur votre machine.
*   npm (Node Package Manager).

Installation & Mise en route :
------------------------------

1.  **Cloner le dépôt** :
    
    bash
    
    ```bash
    git clone "https://gitlab.com/Alexistoribio/wik-dps-tp01.git"
    cd WIK-DPS-TP01
    ```
    
2.  **Initialisation** : Installez les dépendances nécessaires et préparez votre projet :
    
    bash
    
    ```bash
    npm init -y
    npm install typescript --save-dev
    npm install @types/node --save-dev
    ```
    
3.  **Configuration TypeScript** :
    
    bash
    
    ```bash
    npx tsc --init --rootDir src --outDir build --esModuleInterop --resolveJsonModule --lib es6 --module commonjs --allowJs true --noImplicitAny true
    mkdir src
    ```
    
4.  **Démarrez l'API** :
    
    bash
    
    ```bash
    npx tsc
    node build/index.js
    ```
    
    Après cette commande, vous devriez voir un message disant : `Server listening on port 8080`.
    
5.  **Testez l'API** : Dans votre navigateur ou à l'aide d'un outil comme Postman, accédez à :
    
    bash
    
    ```bash
    http://localhost:8080/ping
    ```
    
    Vous devriez recevoir une réponse JSON contenant les headers de votre requête.
    

Documentation API :
-------------------

### GET `/ping`

*   **Réponse** : Renvoie les headers de la requête au format JSON.
*   **Code de statut** : `200 OK` pour une requête réussie, `404 Not Found` pour tout autre endpoint ou méthode.
---