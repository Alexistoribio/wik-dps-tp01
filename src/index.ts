import { createServer, IncomingMessage, ServerResponse } from 'http';

const PORT: number = process.env.PING_LISTEN_PORT ? parseInt(process.env.PING_LISTEN_PORT) : 8080;

const server = createServer((req: IncomingMessage, res: ServerResponse) => {
    if (req.method === 'GET' && req.url === '/ping') {
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(req.headers));
    } else {
        res.writeHead(404);
        res.end();
    }
});

server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
